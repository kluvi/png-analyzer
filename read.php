<?php
$showHex = false;
$files = array(
'body-search.png', 'banner_1.png', 'boty.png',
'1x1px-paleta.png', '1x1px-plnobarevny_RGB.png', '1x1px-RGBA.png', '1x1px-stupne_sedi.png', 'lena-filter-average.png', 'lena-filter-none.png',
'lena-filter-paeth.png', 'lena-filter-sub.png', 'lena-filter-up.png', 'lena-IHDR-IDAT-IEND.png', 'lena-IHDR-IDAT-IEND-gAMA.png', 'lena-IHDR-IDAT-IEND-pHYs.png',
'lena-IHDR-IDAT-IEND-pHYs-tIME.png', 'lena-IHDR-IDAT-IEND-pHYs-tIME-bKGD.png', 'lena-IHDR-IDAT-IEND-tEXt.png');

$files = glob('./*.png');
foreach($files as $k=>$v)
	$files[$k] = basename($v);

if(isset($_GET['file']) && in_array($_GET['file'], $files))
	$file = $_GET['file'];
else
	$file = $files[0];
	
if(isset($_GET['showHex']))
  $showHex = true;
  
echo '<form method="get"><select name="file">';
foreach($files as $f)
	echo '<option value="'.$f.'" '.($_GET['file'] == $f ? 'selected="selected"' : '') .'>'.$f.'</option>';
echo '</select><input type="checkbox" id="showHex" name="showHex"'.($showHex ? 'checked="checked"' : '').' value="1" />
<label for="showHex">show hexa values</label></lable>
<input type="submit" value="Analyzovat" /></form><hr /><b>Analyzovany soubor: </b>';

echo $file;
echo '<br /><img src="'.$file.'" />';
echo '<hr />';

$f = fopen($file, 'r');
$header = fread($f, 8);
echo '<b>Hlavicka</b><br />';
echo hex($header);
echo '<hr />';

$c = true;
$chunks = array();
$i = 0;
while($c)
{
  $len = fread($f, 4);
  $chunks[$i]['len'] = hexdec(hex($len));
  if($showHex)
    $chunks[$i]['len_hex'] = hex($len);
    
  $name = fread($f, 4);
  $chunks[$i]['name'] = $name;
  if($showHex)
    $chunks[$i]['name_hex'] = hex($name);
    
  if($chunks[$i]['len'] > 0)
  {
    $data = fread($f, $chunks[$i]['len']);
    //$chunks[$i]['data'] = $data;
    if($showHex)
      $chunks[$i]['data_hex'] = hex($data);
    $chunks[$i]['data_readable'] = readable($data, $name, $chunks);
  }
  $crc = fread($f, 4);
  //$chunks[$i]['crc'] = $crc;
  $chunks[$i]['crc_hex'] = hex($crc);
  
  if($chunks[$i]['name'] === 'IEND' || $i > 100)
    $c = false;
  $i++;
}

echo '<pre>';
var_dump($chunks);
echo '</pre>';





function readable($data, $name, $chunks)
{
  if($name == 'IHDR')
    return readable_ihdr($data, $name, $chunks);
	elseif($name == 'IDAT')
    return readable_idat($data, $name, $chunks);
	elseif($name == 'PLTE')
    return readable_plte($data, $name, $chunks);
  elseif($name == 'tEXt')
    return readable_text($data, $name, $chunks);
	return $data;
}

function readable_plte($data, $name, $chunks)
{
  global $showHex;
  return '';
}

function readable_idat($data, $name, $chunks)
{
  global $showHex;
  return hex($data);
	$data = gzinflate($data);
	$data = bin2hex($data);
	$data = str_split($data, 8);
	foreach($data as $key=>$val)
	{
    $data[$key] = implode(' ', str_split($val, 2));
  }
  $data = implode('', $data);
	return $data;
}

function readable_text($data, $name, $chunks)
{
  global $showHex;
  $ret = array();
  $data = hex($data);
  $data = explode('00', $data);
  $isKey = true;
  $lastKey = '';
  foreach($data as $row)
  {
    $row = trim(str_replace(' ', '', $row));
    if($isKey)
    {
      $lastKey = hex2str($row);
      $isKey = false;
    }
    else
    {
      $ret[$lastKey] = hex2str($row);
      $isKey = true;
    }
  }
  return $ret;
}

function readable_ihdr($data, $name, $chunks)
{
  global $showHex;
  $ret = array();
	$width = hex($data, array(1, 4));
	if($showHex)
	  $ret['width_hex'] = $width;
	$ret['width'] = hexdec($width);

  $height = hex($data, array(5, 8));
  if($showHex)
	  $ret['height_hex'] = $height;
	$ret['height'] = hexdec($height);

  $bit_hloubka = hex($data, 9);
  if($showHex)
	  $ret['bit_hloubka_hex'] = $bit_hloubka;
	$ret['bit_hloubka'] = (int)hexdec($bit_hloubka);

  $kodovani = hex($data, 10);
  if($showHex)
	  $ret['kodovani_barev_hex'] = $kodovani;
	$ret['kodovani_barev'] = (int)hexdec($kodovani);
	$ret['kodovani_barev_text'] = get_kodovaniBarev($ret['kodovani_barev']);

  $komprimace = hex($data, 11);
  if($showHex)
	  $ret['komprimace_hex'] = $komprimace;
	$ret['komprimace'] = (int)hexdec($komprimace);
	$ret['komprimace_text'] = $ret['komprimace'] == 0 ? 'deflate' :  'unknown';

  $filtrace = hex($data, 12);
  if($showHex)
	  $ret['filtrace_hex'] = $filtrace;
	$ret['filtrace'] = (int)hexdec($filtrace);
	$ret['filtrace_text'] = $ret['filtrace'] == 0 ? 'adaptivni' :  'unknown';

  $prokladani = hex($data, 13);
  if($showHex)
	  $ret['prokladani_hex'] = $prokladani;
	$ret['prokladani'] = (int)hexdec($prokladani);
	if($ret['prokladani'] == 1)
		$ret['prokladani_text'] = 'prokladani';
	elseif($ret['prokladani'] == 0)
		$ret['prokladani_text'] = 'bez prokladani';
	else
		$ret['prokladani_text'] = 'neznamy';
	return $ret;
}

function hex2str($hex)
{
  for($i=0;$i<strlen($hex);$i+=2)
  {
    $str.=chr(hexdec(substr($hex,$i,2)));
  }
  return $str;
}

function get_kodovaniBarev($val)
{
	switch($val)
	{
		case 0:
			return 'Stupne sedi';
		case 2:
			return 'Plnobarevny - true color';
		case 3:
			return 'Obrazek s barevnou paletou';
		case 4:
			return 'Obrazek ve stupnich sedi a s pruhlednosti';
		case 6:
			return 'Kazdy pixel obsahuje RGBA';
	}
}

function hex($val, $ret = NULL, $separator = ' ')
{
  $r1 = bin2hex($val);
  $r2 = str_split($r1, 2);
  if($ret !== NULL)
  {
    if(is_array($ret))
    {
      $str = '';
      for($i=$ret[0]-1;$i<$ret[1];$i++)
        $str .= $r2[$i];
      return $str;
    }
    else
      return $r2[$ret-1];
  }
  return implode($separator, $r2);
}
?>